#include"utils.h"

int main(int argc, char *argv[]  ){   
    if(argc != 2){
        printf("Invalid sintax\n");
        printf("Usage:\n");
        printf("$./reverse 'Hello world'\n");
        printf("world Hello\n");
        exit(0);
    }
    reverse( argv[1]);
    printf("%s\n", argv[1]);   
}
