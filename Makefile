a_OBJS:= reverse.o utils.o
b_OBJS:= test.o utils.o
CC = gcc
ALL = reverse test 
FLAGS = -g -Wall


default: clean reverse



all: $(ALL)

reverse: $(a_OBJS)
	$(CC)  $(FLAGS) -o$@ $^

test: $(b_OBJS)
	$(CC) $(FLAGS) -o$@ $^ 
	./test

%.o: %.c
	$(CC)  $(FLAGS) -c -o$@ $<

clean:
	rm -f *.o $(ALL)

.PHONY: all clean