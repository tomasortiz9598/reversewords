
#include"utils.h"

void reverse(char *String){
    int start = 0, end = 0;
    int length = strlen(String);

    reverseWithLimit(String, 0, length - 1);

    for (int i = 0; i < length; i++){
        if (String[i] == ' '){
            reverseWithLimit(String, start, end);
            start = end = i + 1;
        }
        else
            end = i;
    }
    reverseWithLimit(String, start, end);
 
    
}

void reverseWithLimit(char *String, int start, int end){
    while (start < end)
        change_letters(String, start++, end--);
}

void change_letters(char *String, int i, int j){
    char ch = String[i];
    String[i] = String[j];
    String[j] = ch;
}

