#include"utils.h"
#include<assert.h>

int main(void ){
    char String[] = "today is the first day of the rest of my life";
    char EmptyString[] = "";
    reverse(String);
    reverse(EmptyString);
    assert(!strcmp(EmptyString, ""));
    assert(!strcmp(String, "life my of rest the of day first the is today"));
    assert(strcmp(String, "today is the first day of the rest of my life"));
}
