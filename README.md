# ReverseWords

## Problem:
```
Write a program in C or in Go to reverse the word in a given sentence. For example,
input: today is the first day of the rest of my life
output: life my of rest the of day first the is today

Please be mindful about all aspects of your code (tests, tidiness, etc)
```

## Solution:
### Usage:
```
make 
$./reverse "Hello world"
world Hello
```

### Testing:
```
make test
```

### CI/CD Pipelines:
    Build and test scripts runs everytime i push a new feature
